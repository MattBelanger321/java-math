# Our Trig

This is a custom implementation of some trigonometric function. We have created a class called OurTrig that has six core methods: sin(double x), sin_deg(double angdeg), cosine(double x), cosine_deg(double angdeg), tan(double x), tan_deg(double angdeg). There are also two helper functions called power(double num, double coff) and factorial(double coff) that are implemented. 

To test our class, the following test cases have been implemented:
1. testSin() : used to compute the correct value of sin function given the angle 
2. testCosine() : used to compute the correct value of cosine function given the angle
3. testTan() : used to compute the correct value of tan function given the angle
4. testSin_deg() : used to find sin of a given angle in degrees
5. testCosine_deg() : used to find cosine of a given angle in degrees
6. testTan_deg() : used to find tan of a given angle in degrees

In the versions folder, we have all the test cases versions numbered along with the output of that in a screen capture. Following are the output screenshot files corresponding to each test case:
- OurTrig1.java -> output1.png
- OurTrig2.java -> output2.png
- OurTrig3.java -> output3.png
- OurTrig4.java -> output4.png

First of all, we can see that in all our test cases, we used the assertEquals() function. It is the nature of that function to throw an error only when a test case is not working. If we have multiple test cases, it is inconvenient to always see the results and check manually hence, assertEquals() was used. It takes 3 inputs: first value, second value, delta. 

In the screenshots, it is important to observe the JUnit window on the top left corner where we can focus on Runs, Errors and Failures. Runs simply is the total number of test cases we have hence JUnit running those many test cases, errors mean if the test case was not working due to some syntax/other type of error and failures mean the number of not working test cases found, failure.

We can further observe that in the beginning we had 6 failures, then slowly as we added more code into the methods and constructors, the test cases were working.
