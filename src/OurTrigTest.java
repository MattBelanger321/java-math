import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class OurTrigTest {

	/**
	 * OurTrig Tester. Tests all functions: sin, sin_deg, cosine, cosine_deg, tan,
	 * and tan_deg with well known values on a unit circle. This way all 4 quadrants
	 * are tested,and the edge cases where the tangent is evaluated as undefined are
	 * tested.
	 *
	 * @author Matt, Nandini, Conner, Niraj
	 * @since
	 * 
	 *        <pre>
	 * Mar. 16, 2022
	 *        </pre>
	 * 
	 * @version 1.0
	 */

	double delta = 0.0000000000001;
	double pi = OurTrig.PI;

	@Test
	public void testSin() {
		assertEquals((double) 0, OurTrig.sin(0), delta);
		assertEquals((double) 1 / 2, OurTrig.sin(pi / 6), delta);
		assertEquals((double) Math.sqrt(2) / 2, OurTrig.sin(pi / 4), delta);
		assertEquals((double) Math.sqrt(3) / 2, OurTrig.sin(pi / 3), delta);
		assertEquals((double) 1, OurTrig.sin(pi / 2), delta);
		assertEquals((double) Math.sqrt(3) / 2, OurTrig.sin(2 * pi / 3), delta);
		assertEquals((double) Math.sqrt(2) / 2, OurTrig.sin(3 * pi / 4), delta);
		assertEquals((double) 1 / 2, OurTrig.sin(5 * pi / 6), delta);
		assertEquals((double) 0, OurTrig.sin(pi), delta);
		assertEquals((double) -1 / 2, OurTrig.sin(7 * pi / 6), delta);
		assertEquals((double) -Math.sqrt(2) / 2, OurTrig.sin(5 * pi / 4), delta);
		assertEquals((double) -Math.sqrt(3) / 2, OurTrig.sin(4 * pi / 3), delta);
		assertEquals((double) -1, OurTrig.sin(3 * pi / 2), delta);
		assertEquals((double) -Math.sqrt(3) / 2, OurTrig.sin(5 * pi / 3), delta);
		assertEquals((double) -Math.sqrt(2) / 2, OurTrig.sin(7 * pi / 4), delta);
		assertEquals((double) -1 / 2, OurTrig.sin(11 * pi / 6), delta);
		assertEquals((double) 0, OurTrig.sin(2 * pi), delta);

	}

	@Test
	public void testCosine() {
		assertEquals((double) 1, OurTrig.cosine(0), delta);
		assertEquals((double) Math.sqrt(3) / 2, OurTrig.cosine(pi / 6), delta);
		assertEquals((double) Math.sqrt(2) / 2, OurTrig.cosine(pi / 4), delta);
		assertEquals((double) 1 / 2, OurTrig.cosine(pi / 3), delta);
		assertEquals((double) 0, OurTrig.cosine(pi / 2), delta);
		assertEquals((double) -1 / 2, OurTrig.cosine(2 * pi / 3), delta);
		assertEquals((double) -Math.sqrt(2) / 2, OurTrig.cosine(3 * pi / 4), delta);
		assertEquals((double) -Math.sqrt(3) / 2, OurTrig.cosine(5 * pi / 6), delta);
		assertEquals((double) -1, OurTrig.cosine(pi), delta);
		assertEquals((double) -Math.sqrt(3) / 2, OurTrig.cosine(7 * pi / 6), delta);
		assertEquals((double) -Math.sqrt(2) / 2, OurTrig.cosine(5 * pi / 4), delta);
		assertEquals((double) -1 / 2, OurTrig.cosine(4 * pi / 3), delta);
		assertEquals((double) 0, OurTrig.cosine(3 * pi / 2), delta);
		assertEquals((double) 1 / 2, OurTrig.cosine(5 * pi / 3), delta);
		assertEquals((double) Math.sqrt(2) / 2, OurTrig.cosine(7 * pi / 4), delta);
		assertEquals((double) Math.sqrt(3) / 2, OurTrig.cosine(11 * pi / 6), delta);
		assertEquals((double) 1, OurTrig.cosine(2 * pi), delta);
	}

	@Test
	public void testTan() {
		assertEquals((double) 0, OurTrig.tan(0), delta);
		assertEquals((double) Math.sqrt(3) / 3, OurTrig.tan(pi / 6), delta);
		assertEquals((double) 1, OurTrig.tan(pi / 4), delta);
		assertEquals((double) Math.sqrt(3), OurTrig.tan(pi / 3), delta);
		assertEquals(Double.POSITIVE_INFINITY, OurTrig.tan(pi / 2), delta);
		assertEquals((double) -Math.sqrt(3), OurTrig.tan(2 * pi / 3), delta);
		assertEquals((double) -1, OurTrig.tan(3 * pi / 4), delta);
		assertEquals((double) -Math.sqrt(3) / 3, OurTrig.tan(5 * pi / 6), delta);
		assertEquals((double) 0, OurTrig.tan(pi), delta);
		assertEquals((double) Math.sqrt(3) / 3, OurTrig.tan(7 * pi / 6), delta);
		assertEquals((double) 1, OurTrig.tan(5 * pi / 4), delta);
		assertEquals((double) Math.sqrt(3), OurTrig.tan(4 * pi / 3), delta);
		assertEquals(Double.POSITIVE_INFINITY, OurTrig.tan(3 * pi / 2), delta);
		assertEquals((double) -Math.sqrt(3), OurTrig.tan(5 * pi / 3), delta);
		assertEquals((double) -1, OurTrig.tan(7 * pi / 4), delta);
		assertEquals((double) -Math.sqrt(3) / 3, OurTrig.tan(11 * pi / 6), delta);
		assertEquals((double) 0, OurTrig.tan(2 * pi), delta);
	}

	@Test
	public void testSin_deg() {
		assertEquals((double) 0, OurTrig.sin_deg(0), delta);
		assertEquals((double) 1 / 2, OurTrig.sin_deg(30), delta);
		assertEquals((double) Math.sqrt(2) / 2, OurTrig.sin_deg(45), delta);
		assertEquals((double) Math.sqrt(3) / 2, OurTrig.sin_deg(60), delta);
		assertEquals((double) 1, OurTrig.sin_deg(90), delta);
		assertEquals((double) Math.sqrt(3) / 2, OurTrig.sin_deg(120), delta);
		assertEquals((double) Math.sqrt(2) / 2, OurTrig.sin_deg(135), delta);
		assertEquals((double) 1 / 2, OurTrig.sin_deg(150), delta);
		assertEquals((double) 0, OurTrig.sin_deg(180), delta);
		assertEquals((double) -1 / 2, OurTrig.sin_deg(210), delta);
		assertEquals((double) -Math.sqrt(2) / 2, OurTrig.sin_deg(225), delta);
		assertEquals((double) -Math.sqrt(3) / 2, OurTrig.sin_deg(240), delta);
		assertEquals((double) -1, OurTrig.sin_deg(270), delta);
		assertEquals((double) -Math.sqrt(3) / 2, OurTrig.sin_deg(300), delta);
		assertEquals((double) -Math.sqrt(2) / 2, OurTrig.sin_deg(315), delta);
		assertEquals((double) -1 / 2, OurTrig.sin_deg(330), delta);
		assertEquals((double) 0, OurTrig.sin_deg(360), delta);
	}

	@Test
	public void testCosine_deg() {
		assertEquals((double) 1, OurTrig.cosine_deg(0), delta);
		assertEquals((double) Math.sqrt(3) / 2, OurTrig.cosine_deg(30), delta);
		assertEquals((double) Math.sqrt(2) / 2, OurTrig.cosine_deg(45), delta);
		assertEquals((double) 1 / 2, OurTrig.cosine_deg(60), delta);
		assertEquals((double) 0, OurTrig.cosine_deg(90), delta);
		assertEquals((double) -1 / 2, OurTrig.cosine_deg(120), delta);
		assertEquals((double) -Math.sqrt(2) / 2, OurTrig.cosine_deg(135), delta);
		assertEquals((double) -Math.sqrt(3) / 2, OurTrig.cosine_deg(150), delta);
		assertEquals((double) -1, OurTrig.cosine_deg(180), delta);
		assertEquals((double) -Math.sqrt(3) / 2, OurTrig.cosine_deg(210), delta);
		assertEquals((double) -Math.sqrt(2) / 2, OurTrig.cosine_deg(225), delta);
		assertEquals((double) -1 / 2, OurTrig.cosine_deg(240), delta);
		assertEquals((double) 0, OurTrig.cosine_deg(270), delta);
		assertEquals((double) 1 / 2, OurTrig.cosine_deg(300), delta);
		assertEquals((double) Math.sqrt(2) / 2, OurTrig.cosine_deg(315), delta);
		assertEquals((double) Math.sqrt(3) / 2, OurTrig.cosine_deg(330), delta);
		assertEquals((double) 1, OurTrig.cosine_deg(360), delta);
	}

	@Test
	public void testTan_deg() {
		assertEquals((double) 0, OurTrig.tan_deg(0), delta);
		assertEquals((double) Math.sqrt(3) / 3, OurTrig.tan_deg(30), delta);
		assertEquals((double) 1, OurTrig.tan_deg(45), delta);
		assertEquals((double) Math.sqrt(3), OurTrig.tan_deg(60), delta);
		assertEquals(Double.POSITIVE_INFINITY, OurTrig.tan_deg(90), delta);
		assertEquals((double) -Math.sqrt(3), OurTrig.tan_deg(120), delta);
		assertEquals((double) -1, OurTrig.tan_deg(135), delta);
		assertEquals((double) -Math.sqrt(3) / 3, OurTrig.tan_deg(150), delta);
		assertEquals((double) 0, OurTrig.tan_deg(180), delta);
		assertEquals((double) Math.sqrt(3) / 3, OurTrig.tan_deg(210), delta);
		assertEquals((double) 1, OurTrig.tan_deg(225), delta);
		assertEquals((double) Math.sqrt(3), OurTrig.tan_deg(240), delta);
		assertEquals(Double.POSITIVE_INFINITY, OurTrig.tan_deg(270), delta);
		assertEquals((double) -Math.sqrt(3), OurTrig.tan_deg(300), delta);
		assertEquals((double) -1, OurTrig.tan_deg(315), delta);
		assertEquals((double) -Math.sqrt(3) / 3, OurTrig.tan_deg(330), delta);
		assertEquals((double) 0, OurTrig.tan_deg(360), delta);
	}
}