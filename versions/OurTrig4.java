public final class OurTrig4 {
    public static final double PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;

    /***
     * Calculates the sine of a given angle in radians.
     * 
     * @param x An angle value in radians.
     * @return The sine value of x. A value between -1 and 1 inclusive.
     */
    public static double sin(double x) {
        double result = x;
        double coff = 3.0;

        for (int i = 0; i < 50; i++) {
            double p = power(x, coff);
            double f = factorial(coff);

            if (i % 2 == 0) {
                result = result - (p / f);
            } else {
                result = result + (p / f);
            }
            coff = coff + 2;
        }
        return result;
    }

    /**
     * Calculates the sine of a given angle in degrees.
     * 
     * @param angDeg An angle value in degrees.
     * @return The sine value of angDeg. A value between -1 and 1 inclusive.
     */
    public static double sin_deg(double angDeg) {
        return sin(toRadians(angDeg));
    }

    /**
     * Calculates the cosine of a given angle in radians.
     * 
     * @param x An angle value in radians.
     * @return The cosine of angle x. A value between -1 and 1 inclusive.
     */
    public static double cosine(double x) {
        double cosx = sin(PI / 2 - x);
        return cosx;
    }

    /**
     * Calculates the cosine of a given angle in degrees.
     * 
     * @param angDeg An angle value in degrees.
     * @return The cosine value of angDeg. A value between -1 and 1 inclusive.
     */
    public static double cosine_deg(double angDeg) {
        return cosine(toRadians(angDeg));
    }

    /**
     * Calculates the tangent of a given angle in radians.
     * 
     * @param x An angle value in radians.
     * @return The tangent of angle x.A value between -1 and 1 inclusive. Special
     *         cases where the cosine(x) = 0 return Java's '+Infinity' value since
     *         you cannot have a number divided by 0.
     */
    public static double tan(double x) {
        if (x == PI / 2 || x == 3 * PI / 2) {
            return Double.POSITIVE_INFINITY;
        }

        double tanx = sin(x) / cosine(x);
        return tanx;
    }

    /**
     * Calculates the tangent of a given angle in degrees.
     * 
     * @param angDeg An angle value in degrees.
     * @return The tangent value of angDeg. A value between -1 and 1 inclusive.
     *         Special cases where the cosine(x) = 0 return Java's '+Infinity' value
     *         since you cannot have a number divided by 0.
     */
    public static double tan_deg(double angDeg) {
        return tan(toRadians(angDeg));
    }

    /**
     * Calculates a base value raised to a power value
     * 
     * @param num  Base value.
     * @param coff Exponent value.
     * @return The value of num raised to coff.
     */
    public static double power(double num, double coff) {
        double res = num;

        for (int i = 1; i < coff; i++) {
            res = num * res;
        }
        return res;
    }

    /**
     * Calculates the factorial of the given value.
     * 
     * @param coff Value to calculate the factorial on.
     * @return The computed value for the factorial of coff.
     */
    public static double factorial(double coff) {
        if (coff == 0) {
            return 1;
        }
        return coff * factorial(coff - 1);
    }

    /**
     * Converts degrees to radians.
     * 
     * @param angDeg A value in degrees.
     * @return The radian value of x degrees.
     */
    public static double toRadians(double angDeg) {
        return angDeg * PI / 180;
    }
}